package sample.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ButtonLoginPressedController implements Initializable {

    @FXML
    private void openHomeWindow (ActionEvent event) throws IOException {
        System.out.println("Login button pressed");
        Parent parent = FXMLLoader.load(ButtonLoginPressedController.class.getResource("../views/home.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Home Window");
        stage.setScene(new Scene(parent));
        stage.show();

        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
